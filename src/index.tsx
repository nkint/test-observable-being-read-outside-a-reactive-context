import React from 'react';
import ReactDOM from 'react-dom';
import { configure } from 'mobx';
import { App } from './components/app';
import * as serviceWorker from './serviceWorker';
import { createBrowserHistory } from 'history';
import { syncHistoryWithStore } from 'mobx-react-router';
import { Router } from 'react-router';
import { Routes } from './routes';
import { state } from './store/state';
import 'normalize.css';
import './index.css';

const browserHistory = createBrowserHistory();
const history = syncHistoryWithStore(browserHistory, state.routing);

ReactDOM.render(
  <Router history={history}>
    <App>
      <Routes />
    </App>
  </Router>,
  document.getElementById('root')
);

// MobX configuration
configure({
  enforceActions: 'always',
  observableRequiresReaction: true,
  reactionRequiresObservable: true,
});

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
