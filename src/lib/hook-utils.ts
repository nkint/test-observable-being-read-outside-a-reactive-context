import { createContext, useContext, useEffect, useState, useCallback, useMemo } from 'react';
import { state, State } from '../store/state';

const storeContext = createContext(state);
export const useGlobalState = () => useContext<State>(storeContext);

export function useMount(fn: React.EffectCallback) {
  // https://twitter.com/ryanflorence/status/1125041041063665666
  useEffect(fn, []);
}

export function useToggle(initialState: boolean = false) {
  // https://github.com/bsonntag/react-use-toggle/blob/master/src/index.js
  const [state, setState] = useState<boolean>(initialState);
  const toggle = useCallback(() => setState(state => !state), []);
  return [state, toggle] as [boolean, () => void];
}

// inspired by https://github.com/kitze/react-hanger/blob/master/src/array/useArray.ts
// but with modifyElementById
export function useArray<T extends { id: string }>(initial: T[]) {
  const [value, setValue] = useState(initial);

  const removeById = useCallback(
    (id: string) => setValue(arr => arr.filter(v => v && v.id !== id)),
    []
  );
  const modifyElementById = useCallback(
    (id: string, newValue: Partial<T>) =>
      setValue(arr => arr.map(v => (v.id === id ? { ...v, ...newValue } : v))),
    []
  );
  const replace = useCallback((newArray: T[]) => setValue(newArray), []);
  const push = useCallback((a: T) => {
    setValue(v => [...v, ...(Array.isArray(a) ? a : [a])]);
  }, []);

  const actions = useMemo(
    () => ({
      removeById,
      modifyElementById,
      replace,
      push,
    }),
    [removeById, modifyElementById, replace, push]
  );

  return useMemo(
    () => ({
      value,
      ...actions,
    }),
    [actions, value]
  );
}
