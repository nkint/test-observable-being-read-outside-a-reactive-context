import React from 'react';
import ErrorBoundary from 'react-error-boundary';
import { History, Location } from 'history';
import { Switch, Route, Redirect } from 'react-router';
import { SignIn } from './components/sign-in';

export interface RouterProps<T> {
  history: History;
  location: Location;
  match: T;
}

function RedirectToHomepage() {
  return <Redirect to="/" />;
}

const FallbackComponentReload = () => <div>error</div>;

function withHeader(Component: React.FC<RouterProps<any>>) {
  return (props: RouterProps<any>) => {
    return (
      <div>
        <nav>header</nav>
        <hr />
        <ErrorBoundary FallbackComponent={FallbackComponentReload} onError={console.error}>
          <Component {...props} />
        </ErrorBoundary>
      </div>
    );
  };
}

export function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={withHeader(SignIn)} />
      <Route component={RedirectToHomepage} />
    </Switch>
  );
}
