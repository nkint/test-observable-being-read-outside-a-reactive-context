import { observable, action, computed } from 'mobx';
import { State, RootStore } from './state';

export class User implements RootStore {
  @observable email: string = null;
  @observable password: string = null;
  @observable credentialValidity = observable.map<'email' | 'password', boolean>(
    new Map([['email', false], ['password', false]])
  );

  constructor(public root: State) {}

  @action.bound
  setCredentials(key: 'email' | 'password' | 'code', value: string, valid?: boolean) {
    this[key] = value;
    if (key !== 'code') {
      this.credentialValidity.set(key, Boolean(valid));
    }
  }

  @computed
  get isEmailValid() {
    return this.credentialValidity.get('email');
  }
  @computed
  get isPasswordValid() {
    return this.credentialValidity.get('password');
  }

  @action.bound
  clearCredential() {
    this.email = null;
    this.password = null;
  }

  @action.bound
  signIn() {
    console.log('fetch token');
  }
}
