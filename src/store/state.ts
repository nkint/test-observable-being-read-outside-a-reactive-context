import { RouterStore } from 'mobx-react-router';
import { User } from './user';

export class State {
  user: User = null;
  routing: RouterStore;

  constructor(routing: RouterStore = new RouterStore()) {
    this.routing = routing;

    this.user = new User(this);
  }
}
export const state = new State();

export interface RootStore {
  root: State;
}

(window as any).state = state;
