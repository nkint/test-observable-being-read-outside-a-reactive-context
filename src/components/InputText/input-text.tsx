import React, { useState } from 'react';
import { Eye } from '../../icons/eye';
import uuid from 'uuid/v1';
import { useMount, useToggle } from '../../lib/hook-utils';
import { InputType, ValidateResult } from './why-is-invalid';
import cn from 'classnames';
import './index.css';
import { noop } from 'lodash';

export function eventValueExtractor(fn: (x: string) => void = noop) {
  if (fn === null) return null;
  return (event: React.ChangeEvent<HTMLInputElement>) => fn(event.currentTarget.value);
}

export type InputTextProps = {
  label: string;
  value: string;
  onChange: (x: string, isValid?: boolean) => void;
  onBlur?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  required?: boolean;
  type?: InputType;
  validateResult?: ValidateResult;
};

export const InputText: React.FC<InputTextProps> = ({
  label,
  value = '',
  onChange,
  onBlur,
  required = false,
  type = 'text',
  validateResult = null,
}) => {
  const [id, setId] = useState<string>(null);
  const [visible, toggleVisible] = useToggle(false);

  useMount(() => {
    setId(uuid());
  });

  const typeWithVisibility = type !== 'password' ? type : visible ? 'text' : 'password';

  return (
    <div className="input-text">
      <label htmlFor={id} className="small">
        {label}
      </label>
      <div className={cn('input-text-box', validateResult ? validateResult.type : 'not-validable')}>
        <input
          id={id}
          required={required}
          name={label}
          type={typeWithVisibility}
          value={value}
          onChange={eventValueExtractor((x = '') => onChange(x))}
          onBlur={onBlur}
        />

        {type === 'password' && (
          <Eye barred={visible} onClick={toggleVisible} role="button" className="eye-icon" />
        )}
      </div>
      <ul className="validation-error small">
        {validateResult &&
          validateResult.type === 'invalid' &&
          validateResult.errors.map(errorMessage => <li key={errorMessage}>{errorMessage}</li>)}
      </ul>
    </div>
  );
};
