import { isNil } from 'lodash';
import { email } from '@hapi/address';

export type InputType = 'text' | 'password' | 'email';

export const ERROR_CODES = {
  PASSWORD: {
    AT_LEAST_8_CHARS: 'At least 8 characters',
    DIGITS: 'Must contains a digit',
    CAPITAL: 'Must contains a capital letter',
    LOWERCASE: 'Must contains a lowercase letter',
    SYMBOL: 'Must contains a symbol',
  },
  EMAIL: {
    GENERIC: 'Insert a valid email.',
    TOO_LONG: 'Email address too long.',
    ONLY_ONE_AT_SYMBOL: 'Email must contains only one `@`',
    NO_SPACE: 'Email must not contain space',
    LOCAL_PART_NOT_EMPTY: 'Email local part can not be empty',
    DOMAIN_NOT_EMPTY: 'Email domain can not be empty',
    LOCAL_PART_FIRST_LAST_NOT_DOT: 'Email local part first or last character must not be a `.`',
    LOCAL_PART_NOT_TWO_DOT: "Email local part don't allow two consecutive dot `.`",
    DOMAIN_NOT_TWO_DOT: "Email domain don't allow two consecutive dot `.`",
    DOMAIN_ONE_DOT: 'Email domain must contains at least one `.`.',
    NO_SYMBOL: 'Email must not contains special character',
    NO_UNICODE: 'Email must not contains forbidden Unicode characters',
  },
};

const errorsAndRegex: [string, () => RegExp][] = [
  [ERROR_CODES.PASSWORD.AT_LEAST_8_CHARS, () => /.{8,}$/],
  [ERROR_CODES.PASSWORD.DIGITS, () => /\d+/],
  [ERROR_CODES.PASSWORD.CAPITAL, () => /[A-Z]+/],
  [ERROR_CODES.PASSWORD.LOWERCASE, () => /[a-z]+/],
  // eslint-disable-next-line
  [ERROR_CODES.PASSWORD.SYMBOL, () => /[`~\!@#\$%\^\&\*\(\)\-_\=\+\[\{\}\]\\\|;:'",<.>\/\?€£¥₹]+/],
];

export type ValidateResult =
  | { value: string; type: 'valid'; isValid: true }
  | { value: string; type: 'invalid'; isValid: false; errors: string[] }
  | { value: string; type: 'not-validable'; isValid: null };

export function validate(type: InputType, value: string): ValidateResult {
  const types = ['text', 'password', 'email'];
  if (!types.includes(type)) {
    throw new TypeError(`Invalid type. Type must be [${types.join(', ')}] instead "${type}" found`);
  }

  if (isNil(value) || value === '') {
    return { value, type: 'not-validable', isValid: null };
  }

  let errors: string[] = [];
  if (type === 'email') {
    const codeError = email.analyze(value);
    errors = [codeError && codeError.error].filter(Boolean);
  } else if (type === 'password') {
    errors = errorsAndRegex.reduce((acc, messageRegex) => {
      const [message, reg] = messageRegex;
      if (!reg().test(value)) {
        acc.push(message);
      }
      return acc;
    }, []);
  }
  return errors.length
    ? { value, type: 'invalid', isValid: false, errors }
    : { value, type: 'valid', isValid: true };
}
