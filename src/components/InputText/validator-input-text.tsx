import React, { useState, useRef, useCallback } from 'react';
import { ValidateResult, validate } from './why-is-invalid';
import './index.css';
import { InputTextProps, InputText, eventValueExtractor } from './input-text';

const NOT_VALIDABLE_STARTING_STATE: ValidateResult = {
  value: null,
  type: 'not-validable',
  isValid: null,
};

export const ValidatorInputText: React.FC<InputTextProps> = ({
  label,
  value,
  onChange,
  required,
  type,
}) => {
  const [validateResult, setValidateResult] = useState<ValidateResult>(
    NOT_VALIDABLE_STARTING_STATE
  );

  const timerId = useRef<NodeJS.Timer>(null);

  const handleBlur = useCallback(
    eventValueExtractor((x = '') => {
      const validateResultChanged = validate(type, x);
      setValidateResult(validateResultChanged);
      onChange(validateResultChanged.value, validateResultChanged.isValid);
      clearTimeout(timerId.current);
      timerId.current = null;
    }),
    []
  );

  const handleChange = (x = '') => {
    setValidateResult(NOT_VALIDABLE_STARTING_STATE);
    clearTimeout(timerId.current);
    onChange(x);

    timerId.current = setTimeout(() => {
      const validateResultChanged = validate(type, x);
      setValidateResult(validateResultChanged);
      onChange(x, validateResultChanged.isValid);
    }, 2000);
  };

  return (
    <InputText
      value={value}
      label={label}
      required={required}
      type={type}
      onChange={handleChange}
      onBlur={handleBlur}
      validateResult={validateResult}
    />
  );
};
