import React from 'react';
import { observer } from 'mobx-react-lite';

const AppInner: React.FC = ({ children }) => {
  return <div>{children}</div>;
};

export const App = observer(AppInner);
