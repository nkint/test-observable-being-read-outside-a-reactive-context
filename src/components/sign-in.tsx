import React from 'react';
import { observer } from 'mobx-react-lite';
import { ValidatorInputText as ValidatorInputTextUnobserved } from './InputText';
import { useGlobalState } from '../lib/hook-utils';
import { noop } from 'lodash';

type Evt = React.MouseEvent<HTMLElement> | React.FormEvent<HTMLFormElement>;
export function preventDefault(fn: (event: Evt) => void = noop) {
  return function(event: Evt) {
    event.preventDefault();
    fn(event);
  };
}

const ValidatorInputText = ValidatorInputTextUnobserved;

/** the follow causes a lot of `created/updated without reading any observable value` warning  */
// const ValidatorInputText = observer(ValidatorInputTextUnobserved);

const SignInInner: React.FC = () => {
  const { user } = useGlobalState();
  const { email, password, setCredentials, signIn } = user;
  const isSubmitActive = user.isPasswordValid && user.isEmailValid;

  return (
    <section style={{ margin: '0 50px' }}>
      <h1>Sign in</h1>
      <form onSubmit={preventDefault()}>
        <ValidatorInputText
          label={'Email'}
          value={email || ''}
          onChange={(x, isValid) => {
            setCredentials('email', x, isValid);
          }}
          type="email"
          required
        />
        <ValidatorInputText
          label={'Password'}
          value={password || ''}
          onChange={(x, isValid) => {
            setCredentials('password', x, isValid);
          }}
          type="password"
          required
        />
        <input type="submit" value="Sign in" onClick={() => signIn()} disabled={!isSubmitActive} />
      </form>
    </section>
  );
};

export const SignIn = observer(SignInInner);
