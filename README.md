# Test Observable being read outside a reactive context

Bootstrapped with [CRA](https://github.com/facebook/create-react-app)

Install and run

```sh
npm install
npm start
```

## How to reproduce the behavior

- Open the inspector console
- Click in the form, type an email and a password and click on Sign in button
- Reload the page
- Click in the form to get focus

See the warning:

![warning](screenshot.png)

## Notes

- Everything seems to happen in `src/components/sign-in.tsx` file
- Use `const ValidatorInputText = observer(ValidatorInputTextUnobserved);` does not resolve issue but instead get a lot of _created/updated without reading any observable value_ (and this makes sense to me)
- Seems caused by storing observable variable into a local react state and use it inside `useEffect` because if I use `InputText` instead of `ValidatorInputText` the warning does not appear.
